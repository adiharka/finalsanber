@extends('master')

@section('content')
    
<div class="mt-3 mr-3 ml-3">
    <a href="/create" class="btn btn-primary mt-3">Buat Postingan Baru</a>
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    <table class="table mt-3">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Text</th>
                <th scope="col">Photo</th>
                <th scope="col">Caption</th>
                <th scope="col">Quote</th>
                <th scope="col">ID Pemilik</th>
                <th scope="col" style="display: flex">Actions</th>
            </tr>
        </thead>
        <tbody>
            
            @forelse ($posts as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->text}}</td>
                <td><img src={{$value->photo}} ></td>
                {{-- <td>{{$value->photo}}</td> --}}
                <td>{{$value->caption}}</td>
                <td>{{$value->quote}}</td>
                <td>{{$value->user_id}}</td>
                <td>
                    <div class="d-flex flex-row bd-highlight">
                        <a href="/{{$value->id}}" class="btn btn-info mt-1 mr-1">Show</a>
                        <a href="/{{$value->id}}/edit" class="btn btn-primary mt-1 mr-1">Edit</a>
                        <form action="/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger mt-1" value="Delete">
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
            @endforelse
        </tbody>
    </table>

</div>

@endsection