@extends('master')

@section('heading')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
</div>
@endsection

@section('content')

<div class="ml-3 mr-3 pt-3">
    <h2>Show Post {{$post->id}}</h2>
    <p>{{$post->text}}</p>
    <p>{{$post->caption}}</p>
    <p>{{$post->quote}}</p>
</div>
    
@endsection