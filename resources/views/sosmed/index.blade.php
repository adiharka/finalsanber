@extends('master')
	
@section('content')
<div class="mt-3 mr-3 ml-3">
    @if(session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
    @endif
    <a href="/sosmed/create" class="btn btn-primary mt-3">Ajukan Pertanyaan</a>
    <table class="table mt-3">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Body</th>
                <th scope="col" style="display: flex">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($post as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->judul}}</td>
                <td>{{$value->isi}}</td>
                <td>
                    <div class="d-flex flex-row bd-highlight">
                        <a href="/pertanyaan/{{$value->id}}" class="btn btn-info mt-1 mr-1">Show</a>
                        <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary mt-1 mr-1">Edit</a>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger mt-1" value="Delete">
                        </form>
                    </div>
                </td>
            </tr>
            @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
            @endforelse
        </tbody>
    </table>

</div>

@endsection
