@extends('master')

@section('content')

<div class="ml-3 mr-3 pt-3">
    <h2>Show Post {{$post->id}}</h2>
    <h4>{{$post->judul}}</h4>
    <p>{{$post->isi}}</p>
</div>
    
@endsection