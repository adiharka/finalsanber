<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    // protected $guarded = [];  
    protected $table = 'posts';
    protected $fillable = [
        'text',
        'photo',
        'caption',
        'quote',
        'user_id'
    ];
    public function profile_likes()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
                User::class,
                'post_likes',
                'post_id',
                'profile_id'
            );
    }
}
