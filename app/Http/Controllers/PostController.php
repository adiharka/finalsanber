<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use User;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $posts = Post::all();
        return view('profile.index', compact('user','posts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'body' => 'required',
            // 'img' => 'required',
            'caption' => 'required',
            'quote' => 'required',
        ]);
        
        $post = Post::create ([
            "text" => $request['body'],
            "photo" => $request['img'],
            "caption" => $request['caption'],
            "quote" => $request['quote'],
            "user_id" => Auth::id(),
        ]);

        return redirect('/')->with('success', 'Post berhasil diajukan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Pertanyaan::find($id);
        return view('profile.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Pertanyaan::find($id);
        return view('profile.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:45',
            'isi' => 'required',
        ]);

        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request['judul'],
            "isi" => $request['isi']
        ]);
        return redirect('/')->with('success', 'Post berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pertanyaan::destroy($id);
        return redirect('/');
    }
}
